class Family < ApplicationRecord
  belongs_to :animal
  belongs_to :familiar, class_name: "Animal"
end
