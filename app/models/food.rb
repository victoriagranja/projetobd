class Food < ApplicationRecord
    has_many :animal_foods
    has_many :animals, through: :animal_foods

    before_create :diz_oi

    private
    def diz_oi
        puts "---------------------"
        puts "oi"
        puts "---------------------"
    end
end
