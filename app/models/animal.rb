class Animal < ApplicationRecord
    validates:name, presence: true
    validate :only_adult
    belongs_to:territory

    has_many :animal_foods
    has_many :foods, through: :animal_foods

    has_many :families
    has_many :familiars, through: :families, class_name: "Animal"

    enum kind: {
        Vegetariano: 0,
        Carnivoro: 1
    }

    def age
        ((Date.today - birthdate)/365).to_i
    end

    private
        def only_adult
            if age < 18
                errors.add(:birthdate, " too young")
            end
        end
end
