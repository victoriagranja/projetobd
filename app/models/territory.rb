class Territory < ApplicationRecord
    validates:name, presence:true
    validates:climate, presence:true
    validates:biome, presence:true
    validates:area, presence:true
    has_many :animals
end
