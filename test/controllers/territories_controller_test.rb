require 'test_helper'

class TerritoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @territory = territories(:one)
  end

  test "should get index" do
    get territories_url, as: :json
    assert_response :success
  end

  test "should create territory" do
    assert_difference('Territory.count') do
      post territories_url, params: { territory: { area: @territory.area, biome: @territory.biome, climate: @territory.climate, name: @territory.name } }, as: :json
    end

    assert_response 201
  end

  test "should show territory" do
    get territory_url(@territory), as: :json
    assert_response :success
  end

  test "should update territory" do
    patch territory_url(@territory), params: { territory: { area: @territory.area, biome: @territory.biome, climate: @territory.climate, name: @territory.name } }, as: :json
    assert_response 200
  end

  test "should destroy territory" do
    assert_difference('Territory.count', -1) do
      delete territory_url(@territory), as: :json
    end

    assert_response 204
  end
end
