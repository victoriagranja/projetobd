require 'test_helper'

class AnimalFoodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @animal_food = animal_foods(:one)
  end

  test "should get index" do
    get animal_foods_url, as: :json
    assert_response :success
  end

  test "should create animal_food" do
    assert_difference('AnimalFood.count') do
      post animal_foods_url, params: { animal_food: { animal_id: @animal_food.animal_id, food_id: @animal_food.food_id } }, as: :json
    end

    assert_response 201
  end

  test "should show animal_food" do
    get animal_food_url(@animal_food), as: :json
    assert_response :success
  end

  test "should update animal_food" do
    patch animal_food_url(@animal_food), params: { animal_food: { animal_id: @animal_food.animal_id, food_id: @animal_food.food_id } }, as: :json
    assert_response 200
  end

  test "should destroy animal_food" do
    assert_difference('AnimalFood.count', -1) do
      delete animal_food_url(@animal_food), as: :json
    end

    assert_response 204
  end
end
