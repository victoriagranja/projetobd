Rails.application.routes.draw do
  resources :families
  resources :animal_foods
  resources :foods
  get '/territories/:id/animals', to: 'territories#animals'
  resources :territories
  resources :animals
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/', to:'static_pages#home'
end
