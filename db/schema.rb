# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_23_181441) do

  create_table "animal_foods", force: :cascade do |t|
    t.integer "animal_id"
    t.integer "food_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["animal_id"], name: "index_animal_foods_on_animal_id"
    t.index ["food_id"], name: "index_animal_foods_on_food_id"
  end

  create_table "animals", force: :cascade do |t|
    t.string "name"
    t.date "birthdate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "territory_id"
    t.integer "kind"
    t.index ["territory_id"], name: "index_animals_on_territory_id"
  end

  create_table "families", force: :cascade do |t|
    t.integer "animal_id"
    t.integer "familiar_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["animal_id"], name: "index_families_on_animal_id"
  end

  create_table "foods", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "territories", force: :cascade do |t|
    t.string "name"
    t.string "climate"
    t.string "biome"
    t.float "area"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
