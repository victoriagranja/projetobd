class CreateTerritories < ActiveRecord::Migration[5.2]
  def change
    create_table :territories do |t|
      t.string :name
      t.string :climate
      t.string :biome
      t.float :area

      t.timestamps
    end
  end
end
