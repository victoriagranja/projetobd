class AddTerritoryToAnimal < ActiveRecord::Migration[5.2]
  def change
    add_reference :animals, :territory, foreign_key: true
  end
end
