class CreateFamilies < ActiveRecord::Migration[5.2]
  def change
    create_table :families do |t|
      t.references :animal, foreign_key: true
      t.integer :familiar_id

      t.timestamps
    end
  end
end
